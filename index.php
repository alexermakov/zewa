<html lang="ru-RU">
	<head>
		<title>Zewa</title>
		<meta charset="UTF-8">
		<meta name="robots" content="index, nofollow" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=0">
		<link rel="stylesheet" href="css/normalize.css">
		<link rel="stylesheet" href="css/main.css?3">
	</head>
	<body>
		<div class="main_content__wrap">
			<div class="main_content__inner">
				<div class="main_content">
					<div class="header__main">
						<div class="header__main__info">
							<div class="header__main__title">Календарь заботы о семье</div>
							<div class="header__main__subtitle">
								<a href="#" target="_blank">#ХорошоТам<i>ГдеЗаботятся</i>Вместе</a>
							</div>
							<div class="header__main__text">Используйте для справедливого разделения домашних обязанностей</div>
						</div>
						<div class="header__main__logo">
						</div>
					</div>

					<?php $family = 
						[
							"Папа",
							"Мама",
							"Саша",
							"Вика",
							"Бабушка",
							
						];
						$countFamily = count($family);
					?>

					<?php $schedule = 
						[
							"Купить продукты",
							"Приготовить еду",
							"Помыть посуду",
							"Разложить вещи по местам",
							"Прибрать ванную комнату",
							"Протереть зеркала",
							"Вытереть пыль",
							"Пропылесосить",
							"Постирать одежду",
							"Вынести мусор",
							
						];
						$countSchedule = count($schedule);
					?>
					<div class="main_content__table <?php if ($countSchedule<=4):?> no-gradient<?php endif;?>">
						<!-- шапка таблицы -->
						<div class="main_content__table__head">
							<div class="main_content__table__row<?php if ($countFamily<=4):?>  row--stretch<?php endif;?>">
								<div class="main_content__table__title main_content__table__title--empty"></div>
								<?php foreach ($family as $item):?>
									<div class="main_content__table__cell"><?= $item;?></div>
								<?php endforeach;?>
							</div>
						</div>

						<!-- тело таблицы -->
						<div class="main_content__table__body js_main_content__table__body">
							<?php foreach ($schedule as $item):?>
								<div class="main_content__table__row<?php if ($countFamily<=4):?>  row--stretch<?php endif;?>">
									<div class="main_content__table__title"><?= $item;?></div>
									<?php for ($i=0; $i < $countFamily; $i++):?>
										<div class="main_content__table__cell <?php if (rand(0,1)):?>active<?php endif;?>"></div>
									<?php endfor;?>
									
								</div>
							<?php endforeach;?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script src='js/jquery.min.js'></script>
		<script src='js/main.js'></script>
	</body>
</html>
