$(function () {
    const decoreClassArray = ['active--blue','active--darkblue','active--purple','active--magenta'];

    let countColors = parseInt(decoreClassArray.length);
    let countRows = parseInt($('.js_main_content__table__body .main_content__table__row').length);
    let arrayIndexColor = doArrayColor(countColors,countRows)

    $('.js_main_content__table__body .main_content__table__row').each(function () {
        colorIndex = arrayIndexColor[$(this).index()];
        $(this).find('.main_content__table__cell').each(function () {
            if ($(this).hasClass('active'))
                $(this).addClass(decoreClassArray[colorIndex])
        });
    });

    function doArrayColor(countColors,countRows){
        let arrayColor = [];
        let itterations = Math.trunc(countRows/countColors);

        let rest = countRows % countColors;

        
        for (let j = 0; j < countColors; j++) {
            for (let i = 0; i < itterations; i++) {
                arrayColor.push(j)
            }
            if (rest>0){
                rest--;
                arrayColor.push(j)
            }
        }
        return arrayColor;
    }
});